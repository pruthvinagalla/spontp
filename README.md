Prerequisites:
   Install git tool https://desktop.github.com/                                                                                     
   Install Docker software on your machine https://www.docker.com/community-edition

steps to run the project:
1) clone the repository
2) In Terminal, From project root folder,run below docker commands:                                                                           
   a) docker build -t spontp .  (or)   docker build --no-cache -t spontp .                                                                                              
   b) docker run -it   -v ${PWD}:/usr/src/app   -v /usr/src/app/node_modules   -p 3000:3000 --rm spontp
3) Open your browser to http://localhost:3000/ and you should see the app


steps to run for production:
1) clone the repository
2) From project root folder, run below docker commands:
	a) docker build -t spontp-prod -f DockerfileProd .

	b)docker run -it -p 80:80 -p 443:443 --rm spontp-prod
