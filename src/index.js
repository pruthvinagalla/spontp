import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './font-awesome.css';
import './bootstrap.min.css';
import './App.css';
import './index.css';
import './Resp.css';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
