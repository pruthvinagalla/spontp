import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Navbar from 'react-bootstrap/lib/Navbar.js';
import Nav from 'react-bootstrap/lib/Nav.js';
import NavItem from 'react-bootstrap/lib/NavItem.js';
import logoImg from '../assets/images/logo_img.png';
import mobileLogo from '../assets/images/mlogo.png';
import {NavDropdown,MenuItem} from 'react-bootstrap';
import $ from 'jquery';

class Header extends Component {

  render() {
    return (
      <div>
        <div className="headerrow">
          <div className="logocell">
            <Link to={'/'}>StrongHumanNetwork</Link>
          </div>
          <div className="navcell navigation">
            <Navbar collapseOnSelect>
              <Navbar.Toggle / >
              <Navbar.Collapse >
              <Nav pullRight>
              <NavItem > < li className = {this.props.selected === 'StrongHuman' ? 'active' : ''} role = "presentation" > < Link to = {'/StrongHuman'} > StrongHuman < /Link></li > < /NavItem>
              <NavItem > < li className = {this.props.selected === 'VisualiseTheFuture' ? 'active' : ''} role = "presentation" > < Link to = {'/VisualiseTheFuture'} > Visualise the future < /Link></li > < /NavItem>
              <NavItem > < li className = {this.props.selected === 'WhatDoWeDo' ? 'active' : ''} role = "presentation" > < Link to = {'/WhatDoWeDo'} > What do we do < /Link></li > < /NavItem>
              <NavItem > < li className = {this.props.selected === 'WhyWillWeSucceed' ? 'active' : ''} role = "presentation" > < Link to = {'/WhyWillWeSucceed'} > Why will we succeed < /Link></li > < /NavItem>
              <NavItem > < li className = {this.props.selected === 'Team' ? 'active' : ''}  role = "presentation" > < Link to = {'/Team'} > Team < /Link></li > < /NavItem>
              <NavItem > < li className = {this.props.selected === 'SeedTeam' ? 'active' : ''} role = "presentation" > < Link to = {'/SeedTeam'} > SeedTeam < /Link></li > < /NavItem>
              <NavItem > < li className = {this.props.selected === 'FocusAreas' ? 'active' : ''} role = "presentation" > < Link to = {'/FocusAreas'} > Focus areas < /Link></li > < /NavItem>
              </Nav>
              </Navbar.Collapse>
            </Navbar>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
