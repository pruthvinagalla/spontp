import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import vsec1Img from '../assets/images/vsec01_img.png';
import Telescopeimg from '../assets/images/telescopeImg.png';
import Houseimg from '../assets/images/houseImg.png';
import Alarmimg from '../assets/images/alarmImg.png';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
//import HeaderSignin from './HeaderSignin';
import $ from 'jquery';

class VisualiseTheFuture extends Component {

  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header selected = "VisualiseTheFuture" history = {this.props.history} />
          <section className="vsec01">
            <p className="vbgtitle">VISUALISE THE FUTURE</p>
            <div className="vs01Outcir">
              <div className="vs01Inncir">
                <p className="text-center">VISUALISE THE FUTURE</p>
                <div className="text-center vsec1line"><span className="vsec1lineCir"></span></div>
                <div className="text-center"><img src={vsec1Img} alt="" /></div>
              </div>
            </div>
            <span className="telescope_img"><img src={Telescopeimg} alt="" /></span>
          </section>
          <section className="vsec02">
            <div className="row">
              <div className="col-md-6 col-sm-6 col-xs-12 lftblk">
                <div className="vs02Outcir">
                  <div className="vs02Inncir">
                    <p className="verttxt">01</p>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-6 col-xs-12 rtblk">
                <p className="vbgtitle">TIME</p>
                <p className="f18"><strong>How does my daily routine get affected?<br/>
                   What will we do in our free time?</strong></p>
                <span className="alarm_img"><img src={Alarmimg} alt="" /></span>
              </div>
            </div>
          </section>
          <section className="vsec03">
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 ltblk">
              <p className="vbgtitle">HOUSE</p>
              <p className="text-right pr30 f18"><strong>How will we construct our houses?</strong></p>
              <span className="house_img"><img src={Houseimg} alt="" /></span>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12 rtblk">
              <div className="vs02Outcir">
                <div className="vs02Inncir">
                  <p className="verttxt">02</p>
                </div>
              </div>
            </div>
          </div>
          </section>
          <section className="vsec02 vsec04">
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 lftblk">
              <div className="vs02Outcir">
                <div className="vs02Inncir">
                  <p className="verttxt">03</p>
                </div>
              </div>
            </div>
            <div>
              <p className="vsec04title vbgtitle1">FOOD</p>
              <p className="vsec04title vbgtitle2">SOCIETY</p>
              <p className="vsec04title vsec04title vbgtitle3">MONEY</p>
              <p className="vsec04title vbgtitle4">TECHNOLOGY</p>
              <p className="text-right vtxt f18 divfrnt">
                <strong>What kind of food will we eat?<br/>
                How do we interact with technology?<br/>
                How does my relationship with society transform?<br/>
                How will the role of money evolve?<br/>
                How do various concepts like country, religion evolve?</strong>
              </p>
            </div>
          </div>
          </section>
          <section className="vsec05 hsec05">
            <div className="sec05blk1">
              <h2 className="text-center">THOUGHTS</h2>
              <h3 className="text-center">Have some thoughts to share?<br/>
                Let’s start discussing!</h3>
            </div>
            <div className="sec05blk2">
              <h3 className="text-center">Keep in touch</h3>
              <Footer />
            </div>
          </section>
      </div>
    );
  }
}

export default VisualiseTheFuture;
