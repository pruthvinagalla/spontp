import React, { Component } from 'react';
import Header from './Header';
import { Link } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import Constant from './../../util/Constants';

class ForgotPassword extends Component {
  constructor (props) {
    super(props);
    this.state = {
      email: '',
      emailValid: true,
      showModal: false,
      showModal2: false
    }
  }
  handleUserInput = (e) => {
    this.email = e.target.value;
    let emailValid = this.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    this.setState({ emailValid: emailValid});
    this.setState({ formValid: emailValid});
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    fetch(Constant.backendurl+'/apicalls/forgot_password', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.email
      }),
    })
    .then(response => {return response.text();})
    .then( text => {
                      if(text === 'existing email')
                        window.location.replace('/#/ForgotPasswordInfo/');
                      else{
                            this.setState({ showModal: true })
                            this.setState({errorMessage: "Email does not exist"})
                          }
                      }
          )
    .catch(error => {
                      console.log('parsing failed', error);
                      this.setState({ showModal2: true })
                      this.setState({errorMessage2: "Something went wrong, please try again"})
                    }
          )
  }
  render() {
    return (
      <div>
        <Header />
        <div>
          <Modal show={this.state.showModal}>
            <Modal.Body>
              <span> {this.state.errorMessage} </span>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.setState({ showModal: false, email: "", formValid: false })}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
        <div>
          <Modal show={this.state.showModal2}>
            <Modal.Body>
              <span> {this.state.errorMessage2} </span>
            </Modal.Body>
            <Modal.Footer>
              <Link to={'/'}>  <Button onClick={() => this.setState({ showModal2: false, email: "", password: "", formValid: false })}>Close</Button></Link>
            </Modal.Footer>
          </Modal>
        </div>
            <div className="col-md-12 col-xs-12 forgotpassworddiv">
              <h4 className="text-center">Enter your registered mail id!</h4>
              <form className="form-horizontal form-label-left input_mask" onSubmit={this.handleSubmit.bind(this)}>
                <div className="col-md-3 col-sm-2 col-xs-12"></div>
                <div className="col-md-6 col-sm-8 col-xs-12">
                  <div className="form-group prel">
                    <input type="email" className="form-control" placeholder="Email Id" required onChange={this.handleUserInput.bind(this)}  />
                    <span className="form_error_msg" hidden={this.state.emailValid && true}>Invalid Email</span>
                  </div>
                  <div className="ln_solid"></div>
                  <div className="form-group">
                    <div className="text-right fpdiv">
                      <button type="submit" className="btn btn-success manual_btn" disabled={!this.state.formValid}>Submit</button>
                      <span className="go_to_home"><Link to={'/'}>Go to home page</Link></span>
                    </div>
                  </div>
                </div>
                <div className="col-md-3 col-sm-2 col-xs-12"></div>
              </form>
            </div>
          </div>
    );
  }
}

export default ForgotPassword;
