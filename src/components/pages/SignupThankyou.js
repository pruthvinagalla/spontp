import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import hsec1Img from '../assets/images/hsec01_img.png';
import Navbar from 'react-bootstrap/lib/Navbar.js';
import Nav from 'react-bootstrap/lib/Nav.js';
import NavItem from 'react-bootstrap/lib/NavItem.js';
import Header from './Header';
import $ from 'jquery';

class SignupThankyou extends Component {

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
      <Header />
        <section className="jsec02 hsec02 stq">
        <div className="pagecontent">
          <div className="container">
            <div className="col-md-12 col-xs-12">
              <h4 className="text-center">Thank You!<br/><br/>For activation we sent a link to your registered Email id.</h4><br/>
            </div>
          </div>
        </div>
        </section>


      </div>
    );
  }
}

export default SignupThankyou;
