import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import vsec1Img from '../assets/images/vsec01_img.png';
import Qsymbol from '../assets/images/qsymbol.png';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
import $ from 'jquery';

class WhatDoWeDo extends Component {

  constructor (props) {
    super(props);

  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
    <Header selected = "WhatDoWeDo" history = {this.props.history} />
        <section className="wdsec01 vsec01">
          <p className="vbgtitle">WHAT DO WE DO</p>
          <div className="vs01Outcir">
            <div className="vs01Inncir">
              <p className="text-center">WHAT DO WE DO ?</p>
              <div className="text-center vsec1line"><span className="vsec1lineCir"></span></div>
              <div className="text-center"><img src={vsec1Img} alt="" /></div>
            </div>
          </div>
          <span className="Qimg"><img src={Qsymbol} alt="" /></span>
        </section>
        <section className="wdsec02 vsec05 hsec05">
          <div className="sec05blk1">
            <div className="wdsec02cl1">
              <h4>01</h4>
              <p>Think Tank</p>
              <Link to={'/ThinkTanks'}>Know more</Link>
            </div>
            <div className="wdsec02cl2">
              <h4>02</h4>
              <p>Academy</p>
              <Link to={'/Academy'}>Know more</Link>
            </div>
            <div className="wdsec02cl3">
              <h4>03</h4>
              <p>Business Transformation</p>
              <Link to={'/BusinessTransformation'}>Know more</Link>
            </div>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default WhatDoWeDo;
