import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import hsec1Img from '../assets/images/hsec01_img.png';
import Navbar from 'react-bootstrap/lib/Navbar.js';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
import HeaderSignin from './HeaderSignin';
import $ from 'jquery';

class HomeSignIn extends Component {

  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }
  render() {
    return (
      <div>
        <HeaderSignin history = {this.props.history} />
        <section className="hsec01 hmsn">
          <h1 className="text-center">Are you ready for the future ?</h1>
          <div className="text-center"><span className="sech1Line"><span className="sech1cir"></span></span></div>
          <p className="text-center">
            With the advent of AI and machine learning, machines are becoming smarter.<br/>
            Are you prepared to face the machines?<br/>
            Are you going to be smarter than a machine in 4 years from now?<br/>
            Are you developing your skills to keep yourself competitive?
          </p>
          <div className="text-center"><img className="dwarr" src={hsec1Img} alt="" /></div>
        </section>
        <section className="hsec02">
          <div className="row flrow">
            <div className="col-md-5 col-sm-5 col-xs-12 lftblk seccell02">
              <p className="sec2pTitle">Skill development is the most important subject
                  for anyone on the planet and it is going to impact each one of us</p>
              <div className="text-center divline"><span className="sech2Line"><span className="sech2cir"></span></span></div>
              <p>Although people have started to realize this, we haven't been able to figureout this
                  complex problem of overall skill development.<br/>
                  SHN believes that personality development is the foundation of skill
                  development and is the solution to everything on this planet starting
                  from problems like environmental devastation to genocides to poverty
                  to world peace.</p>
            </div>
            <div className="col-md-7 col-sm-7 col-xs-12 rtblk seccell01">
              <h2 className="hbgtitle">SKILL</h2>
              <div className="frntcir"><span className="verttxt">01</span></div>
              <div className="backcir"></div>
            </div>
          </div>
        </section>
        <section className="hsec02 hsec03">
        <div className="row">
          <div className="col-md-7 col-sm-7 col-xs-12 rtblk">
            <h2 className="hbgtitle">SOLUTION</h2>
            <div className="frntcir"><span className="verttxt">02</span></div>
            <div className="backcir"></div>
          </div>
          <div className="col-md-5 col-sm-5 col-xs-12 lftblk">
            <p className="sec2pTitle text-right">At SHN we have created a model which
                can solve this complex problem in a very
                simple and effective way.</p>
            <div className="text-center divline"><span className="sech2Line"><span className="sech2cir"></span></span></div>
            <p className="text-right">
                StrongHumanNetwork believes in the idea that only StrongHumans will
                survive on the planet and has thus, been envisioned to excel the same purpose.<br/>
                SHN is a personality development focus company.<br/>
                <strong>We make personality development sexy!</strong></p>
          </div>
        </div>
        </section>
        <section className="hsec02 hsec04">
        <div className="row flrow">
          <div className="col-md-5 col-sm-5 col-xs-12 lftblk seccell02">
            <p className="sec2pTitle">StrongHumanNetwork is the platform on which all the
                humans who want to be StrongHumans unite to take
                their future in their hands and shape it in the way
                they want it to be.
            </p>
            <div className="text-center divline"><span className="sech2Line"><span className="sech2cir"></span></span></div>
            <p>In the process of creating StrongHumans, a global discussion has been initiated
                to understand how to handle this large scale transformation in various walks of life.
                People from all over the world have started interacting and understanding how basic
                human necessities like education, healthcare, agriculture, recreation, food,
                entertainment are evolving.</p>
          </div>
          <div className="col-md-7 col-sm-7 col-xs-12 rtblk seccell01">
            <h2 className="hbgtitle">VISION</h2>
            <div className="frntcir"><span className="verttxt">03</span></div>
            <div className="backcir"></div>
          </div>
        </div>
        </section>
        <section className="hsec05">
          <div className="sec05blk1">
            <h2 className="text-center">WELCOME</h2>
            <h3 className="text-center">WELCOME TO THE FUTURE!</h3>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>

            <Footer />

          </div>
        </section>
      </div>
    );
  }
}

export default AuthWrapped(HomeSignIn);
