import React, { Component } from 'react';
import Header from './Header';
import { Link } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import Constant from './../../util/Constants';

class CreatePassword extends Component {
  constructor (props) {
    super(props);
    this.state = {
      password: '',
      confirmpassword: '',
      passwordValid: true,
      confirmpasswordValid: true,
      show1: false,
      show2: false,
      show: false,
      showModal: false,
      showModal2: false,
      showModal3: false,
      type: 'password'
    }
    this.showHide = this.showHide.bind(this);
  }

  showHide(e){
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    })
  }

  handleUserInput = (e) => {
    const name = e.target.id;
    const value = e.target.value;
    this.setState({[name]: value}, () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let passwordValid = this.state.passwordValid;
    let confirmpasswordValid = this.state.confirmpasswordValid;

    switch(fieldName) {
      case 'password':
        this.passwordValue = value;
        passwordValid = value.length>7;
        confirmpasswordValid = value===this.confirmpasswordValue;
        this.setState({show1 : true});
        break;
      case 'confirmpassword':
        this.confirmpasswordValue = value;
        confirmpasswordValid = value===this.passwordValue;
        this.setState({show2 : true});
        break;
      default:
        break;
    }
    this.setState({ passwordValid: passwordValid,
                  confirmpasswordValid: confirmpasswordValid
                }, this.validateForm);
  }

  validateForm() {
    this.setState({show: this.state.show1 && this.state.show2})
    this.setState({formValid: this.state.passwordValid && this.state.confirmpasswordValid && this.state.show});
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'has-error');
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    fetch(Constant.backendurl+'/apicalls/set_reg_pwd', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        password: this.passwordValue,
        ref: this.props.match.params.ref
      }),
    })
   .then(response => {return response.text();})
   .then(text => {
                    if(text === 'valid'){
                      this.setState({showModal3: true});
                    } else{
                      this.setState({ showModal: true })
                      this.setState({errorMessage: "Your activation link is invalid"})
                    }
                  }
        )
  .then(this.setState({ show: true }))
  .catch(error => {
                    console.log('parsing failed', error);
                    this.setState({showModal2: true })
                    this.setState({errorMessage2: "Something went wrong, please try again"})
                  }
        )
  }

  handleDismiss() {
     this.setState({ showModal3: false });
     window.location.replace('/#/Signin');
  }

  render() {
    return (
      <div>
        <Header />
        <div>
          <Modal show={this.state.showModal3}>
            <Modal.Body>
              <span> Your password creation was successful. </span>
            </Modal.Body>
            <Modal.Footer>
              <Button bsStyle="primary" onClick={this.handleDismiss.bind(this)}>SignIn</Button>
            </Modal.Footer>
          </Modal>
        </div>
        <div>
          <Modal show={this.state.showModal}>
            <Modal.Body>
              <span> {this.state.errorMessage} </span>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.setState({ showModal: false, password: "", confirmpassword: "", formValid: false })}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
      <div>
        <Modal show={this.state.showModal2}>
          <Modal.Body>
            <span> {this.state.errorMessage2} </span>
          </Modal.Body>
          <Modal.Footer>
            <Link to={'/'}>  <Button onClick={() => this.setState({ showModal2: false, email: "", password: "", formValid: false })}>Close</Button></Link>
          </Modal.Footer>
        </Modal>
      </div>
      <div className="pagecontent">
        <div className="container cpwpg">
          <div className="col-md-12 col-xs-12">
            <h4 className="text-center">Create your password</h4>
              <form className="form-horizontal form-label-left input_mask" onSubmit={this.handleSubmit.bind(this)}>
                <div className="col-md-3 col-sm-2 col-xs-12"></div>
                <div className="col-md-6 col-sm-8 col-xs-12 fa_pw_blk">
                  <div className="form-group has-feedback fa_pw">
                    <input type={this.state.type} className="form-control has-feedback-left" id="password" value={this.state.password} placeholder="Password"
                                              onChange={this.handleUserInput.bind(this)} />
                    <span onClick={this.showHide}>{this.state.type === 'input' ? <span className="fa fa-eye"></span> : <span className="fa fa-eye-slash"></span>}</span>
                    <span className="form_error_msg" hidden={this.state.passwordValid}>Invalid Password, Check rules below</span>
                  </div>

                  <div className="form-group has-feedback fa_pw">
                      <input type={this.state.type} className="form-control" id="confirmpassword" placeholder="Confirm Password"
                                              value={this.state.confirmpassword}
                                              onChange={this.handleUserInput.bind(this)} />
                      <span className="form_error_msg" hidden={this.state.confirmpasswordValid}>Passwords do not match</span>
                  </div>
                  <div className="form-group">
                    <span>We impose the following password rules:</span><br/>
                    Your password has to be atleast 8  characters long.<br/>
                  </div>
                  <div className="ln_solid"></div>

                  <div className="form-group">
                    <div className="text-right">
                      <button type="submit" disabled={!this.state.formValid} className="btn btn-success manual_btn">Submit</button>
                    </div>
                  </div>
                </div>
                <div className="col-md-3 col-sm-2 col-xs-12"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreatePassword;
