import React, { Component } from 'react';
import Header from './Header';


class ForgotPasswordInfo extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="pagecontent">
          <div className="container fpwinfo">
            <div className="col-md-12 col-xs-12">
              <h4 className="text-center">To reset password we sent a link to your registered mail id.</h4><br/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ForgotPasswordInfo;
