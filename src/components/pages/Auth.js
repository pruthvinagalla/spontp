import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import Constant from './../../util/Constants';

class Auth extends Component {
  constructor(props){
		super(props);
    this.state = {showModal: false}
	}

  componentDidMount () {
    var ref = this.props.match.params.ref;
    fetch(Constant.backendurl+'/apicalls/get_ref?ref='+ref)
      .then(response => {return response.text();})
      .then( text => {
                            if(text === 'valid')
                              window.location.replace('/#/CreatePassword/'+ref);
                            else{
                              this.setState({ showModal: true })
                              this.setState({errorMessage: "Your activation link is invalid"});debugger;
                            }
                          }
           )
      .catch(error => {
                            console.log('parsing failed', error);
                            this.setState({ showModal: true })
                            this.setState({errorMessage: "Something went wrong, please try again"})
                      }
            )
  }


  render() {
    return (
      <div>
        <Modal show={this.state.showModal}>
          <Modal.Body>
            <span> {this.state.errorMessage} </span>
          </Modal.Body>
          <Modal.Footer>
            <Link to={'/'}> <Button onClick={() => this.setState({ showModal: false})}>Close</Button></Link>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Auth;
