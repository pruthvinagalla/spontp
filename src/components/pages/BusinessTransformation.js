import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
//import HeaderSignin from './HeaderSignin';
import $ from 'jquery';

class BusinessTransformation extends Component {

  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header history = {this.props.history} />
        <section className="btsec01 acsec01 wwsec01 hsec01">
          <div className="vs01Outcir">
            <div className="vs01Inncir">
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 ltblk">
              <p className="wws1bgtitle">USINESS</p>
              <p className="wws1txt">BUSINESS TRANSFORMATION</p>
            </div>
            <div className="col-md-6 rtblk divfrnt">
              <p><strong>When people change, their interaction changes. Business is just an interaction between people!</strong></p>
            </div>
          </div>
          <div className="text-center"><img src={hsec1Img} alt="" /></div>
        </section>

        <section className="btsec02 fasec02 wwsec02 hsec02 hsec03">
          <div className="vs02Outcir">
            <div className="vs02Inncir"></div>
          </div>
          <p className="wws1bgtitle">ERTISE</p>
          <p className="wws1txt">OUR AREAS OF EXPERTISE</p>
          <div className="faTitle1">
            <p className="faTxt1">01</p>
            <p className="faTxt2">Food</p>
          </div>
          <div className="faTitle2">
            <p className="faTxt1">02</p>
            <p className="faTxt2">Education and Sports</p>
          </div>
          <div className="faTitle3">
            <p className="faTxt1">03</p>
            <p className="faTxt2">Tourism</p>
          </div>
          <div className="faTitle4">
            <p className="faTxt1">04</p>
            <p className="faTxt2">Healthcare</p>
          </div>
          <div className="faTitle5">
            <p className="faTxt1">05</p>
            <p className="faTxt2">Entertainment</p>
          </div>
          <div className="faTitle6">
            <p className="faTxt1">06</p>
            <p className="faTxt2">Agriculture</p>
          </div>
        </section>

        <section className="btsec03 acsec05 wwsec05 hsec05">
          <div className="text-center sec05blk1">
            <h2>KNOW MORE</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default BusinessTransformation;
