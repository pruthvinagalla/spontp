import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
//import HeaderSignin from './HeaderSignin';
import $ from 'jquery';

class Academy extends Component {
  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header history = {this.props.history} />
        <section className="acsec01 wwsec01 hsec01">
          <div className="vs01Outcir">
            <div className="vs01Inncir">
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 ltblk">
              <p className="wws1bgtitle">CADEMY</p>
              <p className="wws1txt">ACADEMY</p>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12 rtblk divfrnt">
              <p><strong>StrongHumanAcademy is the place which prepares us for the future.
                Academy is designed to take human intellect to its optimum level by reducing
                unnecessary noise which stops us from thinking straight and deeply focussing on
                oneself. It works on a unique concept of noise reduction technology.
                 We focus on peer to peer learning methodology which is ingrained in the belief that everyone
                has something to offer to each other. The effectiveness of the Academy is based
                on a simple principle - being honest to yourself is the best way to lead a quality life!</strong></p>
            </div>
          </div>
          <div className="text-center"><img src={hsec1Img} alt="" /></div>
        </section>
        <section className="acsec02 wwsec02 hsec02 hsec03">
        <div className="row">
          <div className="col-md-6 col-sm-6 col-xs-12 rtblk">
            <h2 className="hbgtitle">IMMERSION</h2>
            <div className="frntcir"><span className="verttxt">01</span></div>
            <div className="backcir"></div>
          </div>
          <div className="col-md-6 col-sm-6 col-xs-12 lftblk">
            <p className="acblkp"><strong>A person who joins the academy will go through a 3 day immersion program into
                the network. The immersion program consists of diverse set of participants
                ranging from fashion designers, managers to graduate students to doctors to
                teenagers to housewives. The program is envisioned to give participant a holistic
                perspective on life. It also acts as an introduction to the network of
                StrongHumans, which acts as the community which will own the participant and
                help in to grow in life across all the aspects.</strong></p>
          </div>
        </div>
        </section>
        <section className="acsec03 wwsec03 hsec02 hsec04">
        <div className="row flrow">
          <div className="col-md-6 col-sm-6 col-xs-12 lftblk seccell02">
              <p className="acblkp"><strong>Post the workshop, the participant is paired with a ChangeMaker who will be
                mentoring the participant for the next two months. A subject matter expert will
                step-in frequently to guide as well as to evaluate the progress. At the end of 2
                month program based on the progress, the participant will be awarded
                StrongHumanCertification.</strong></p>
          </div>
          <div className="col-md-6 col-sm-6 col-xs-12 rtblk seccell01">
            <h2 className="hbgtitle">CERTIFICATION</h2>
            <div className="frntcir"><span className="verttxt">02</span></div>
            <div className="backcir"></div>
          </div>
        </div>
        </section>
        <section className="acsec04 wwsec04 wwsec02 hsec02 hsec03">
        <div className="row">
          <div className="col-md-6 col-sm-6 col-xs-12 rtblk">
            <h2 className="hbgtitle">OPPORTUNITY</h2>
            <div className="frntcir"><span className="verttxt">03</span></div>
            <div className="backcir"></div>
          </div>
          <div className="col-md-6 col-sm-6 col-xs-12 lftblk">
            <p className="acblkp"><strong>Post the certification, the participant can be absorbed into the organization as
                per availability of suitable roles.<br/>
                SHN will also introduce participants to opportunities with its partner organizations.<br/>
                Academy will be regularly conducting workshops(offline & online) on various subjects directly related to an individual’s growth.<br/>
                In Future Academy will be coming up with more challenging certifications to recognize StrongHumans.<br/>
                Academy will evolve into a Centre for human excellence which will stand as a
                symbol for the StrongHuman!</strong></p>
          </div>
        </div>
        </section>
        <section className="acsec05 wwsec05 hsec05">
          <div className="text-center sec05blk1">
            <h2>JOIN THE ACADEMY</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default Academy;
