import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import { Button, Modal } from 'react-bootstrap';
import Constant from './../../util/Constants';
import Footer from './Footer';
import $ from 'jquery';

class JoinUs extends Component {

  constructor (props) {
    super(props);
    this.state = {
      email: '',
      fullname: '',
      gender: '',
      address: '',
      phone: '',
      country: '',
      stateValue: '',
      city: '',
      pincode: '',
      strongHuman: '',
      mail: 'false',
      whatsapp: 'false',
      emailValid: true,
      fullnameValid: true,
      genderValid: false,
      addressValid: false,
      strongHumanValid: false,
      phoneValid: true,
      countryValid: true,
      stateValid: true,
      cityValid: true,
      pincodeValid: true,
      formValid: false,
      show1: false,
      show2: false,
      show3: false,
      show: false,
      showModal: false,
      showModal2: false
    }
  }

  onExitStrongHuman = (e) => {
    let value = e.target.value;
    value = value.replace(/^ +/gm, '');
    this.setState({strongHuman: value});
    if(value === ""){
      this.setState({strongHumanValid: false, formValid: false});
  }
  }
  handleUserInput = (e) => {
    const name = e.target.name;
    let value = e.target.value;
    if( e.target.type === 'checkbox'){
      if( e.target.checked === true){
          value = 'true';
      } else {
          value = 'false';
      }
    }
    if( typeof value === 'string' && name !== 'strongHuman' ){
      value = value.replace(/\s\s+/g, ' ');
    }
    this.setState({[name]: value},
                () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let emailValid = this.state.emailValid;
    let fullnameValid = this.state.fullnameValid;
    let genderValid = this.state.genderValid;
    let addressValid = this.state.addressValid;
    let phoneValid = this.state.phoneValid;
    let countryValid =  this.state.countryValid;
    let stateValid = this.state.stateValid;
    let cityValid = this.state.cityValid;
    let pincodeValid = this.state.pincodeValid;
    let strongHumanValid = this.state.strongHumanValid;


    switch(fieldName) {
      case 'email':
        this.email = value;
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        this.setState({show1 : true});
        break;
      case 'fullname':
        this.name = value;
        fullnameValid = value.match(/^([a-zA-Z ])*$/i) && value.length >=1;
        this.setState({show2 : true});
        break;
      case 'address':
        this.address = value;
        addressValid = value.length > 0;
        break;
      case 'gender':
        this.gender = value;
        genderValid = value.length >= 0;
        break;
      case 'phone':
        this.phone = value;
        phoneValid = value.match(/^\d+$/) && value.length == 10;
        this.setState({show3 : true});
        break;
      case 'country':
        this.country = value;
        countryValid = value.match(/^([a-zA-Z ])*$/i) && value.length >= 1;
        break;
      case 'stateValue':
        this.stateValue = value;
        stateValid = value.match(/^([a-zA-Z ])*$/i) && value.length >= 1;
        break;
      case 'city':
        this.city = value;
        cityValid = value.match(/^([a-zA-Z ])*$/i) && value.length >= 1;
        break;
      case 'pincode':
        this.pincode = value;
        pincodeValid = value.match(/^\d+$/) && value.length == 6;
        break;
      case 'mail':
        this.mail = value;
        break;
      case 'whatsapp':
        this.whatsapp = value;
        break;
      case 'strongHuman':
        this.strongHuman = value;
        strongHumanValid = value.length > 0;
        break;
      default:
        break;
    }

    this.setState({ emailValid: emailValid,
                  fullnameValid: fullnameValid,
                  genderValid: genderValid,
                  addressValid: addressValid,
                  phoneValid: phoneValid,
                  countryValid: countryValid,
                  stateValid: stateValid,
                  cityValid: cityValid,
                  pincodeValid: pincodeValid,
                  strongHumanValid: strongHumanValid
                }, this.validateForm);
  }

  validateForm() {
    this.setState({show: this.state.show1 && this.state.show2 && this.state.show3});
    this.setState({formValid: this.state.emailValid && this.state.fullnameValid && this.state.phoneValid && this.state.addressValid && this.state.strongHumanValid && this.state.countryValid && this.state.stateValid && this.state.cityValid && this.state.genderValid && this.state.pincodeValid && this.state.show});
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'has-error');
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    fetch(Constant.backendurl+'/apicalls/set_reg', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
       name: this.state.fullname,
       email: this.state.email,
       phone: this.state.phone,
       gender: this.state.gender,
       country: this.state.country,
       state: this.state.stateValue,
       city: this.state.city,
       pincode: this.state.pincode,
       address: this.state.address,
       mail: this.state.mail,
       whatsapp: this.state.whatsapp,
       strongHuman: this.state.strongHuman
      }),
  })
  .then(response => {return response.text();})
  .then( text => {
                    if(text === 'existingemail'){
                      this.setState({ showModal: true })
                      this.setState({errorMessage: "Email already registered"});
                      document.getElementById("check1").checked = false;
                      document.getElementById("check2").checked = false;
                      let ele = document.getElementsByName("gender");
                      for(var i=0;i<ele.length;i++)
                        ele[i].checked = false;
                    }
                    else if (text === 'dbissue'){
                      this.setState({ showModal: true })
                      this.setState({errorMessage: "Issue with database"});
                      document.getElementById("check1").checked = false;
                      document.getElementById("check2").checked = false;
                      let ele = document.getElementsByName("gender");
                      for(var i=0;i<ele.length;i++)
                        ele[i].checked = false;
                      } else {
                        window.location.replace('/#/SignupThankyou')
                    }
                  }
        )
  .catch(error => {
                    console.log('parsing failed', error);
                    this.setState({ showModal2: true })
                    this.setState({errorMessage2: "Something went wrong, please try again"})
                  }
        )
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header />
        <div>
          <Modal show={this.state.showModal}>
            <Modal.Body>
              <span> {this.state.errorMessage} </span>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.setState({ showModal: false, fullname: "", email: "",  gender: "", address: "", phone: "",
              country: "", stateValue: "", city: "", pincode: "", mail: 'false', whatsapp: 'false',strongHuman: "", formValid: false })}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
        <div>
          <Modal show={this.state.showModal2}>
            <Modal.Body>
              <span> {this.state.errorMessage2} </span>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.setState({ showModal2: false, fullname: "", email: "",  gender: "", address: "", phone: "",
              country: "", stateValue: "", city: "", pincode: "", mail: 'false', whatsapp: 'false',strongHuman: "", formValid: false })}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
        <section className="jsec02 hsec01">

          <h1 className="text-center">JOIN THE REVOLUTION</h1>
          <div className="text-center"><span className="sech1Line"><span className="sech1cir"></span></span></div>

          <form className="form-horizontal form-label-left input_mask" onSubmit={this.handleSubmit.bind(this)} >
            <div className="col-md-2 col-sm-1 col-xs-12"></div>
            <div className="col-md-4 col-sm-5 col-xs-12 jlblk">

            <div className="form-group has-feedback">
              {/*}  <input type="text" className="form-control has-feedback-left" id="inputSuccess2" placeholder="Name*" />*/}
                <input id="fullname" type="text"  required className="form-control has-feedback-left" name="fullname"
                                        placeholder="Full Name*"
                                        value={this.state.fullname}
                                        onChange={this.handleUserInput.bind(this)} />
                                        <span className="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        <span className="form_error_msg" hidden={this.state.fullnameValid}>Invalid Name</span>
            </div>

            <div className="form-group has-feedback">
              {/*  <input type="text" className="form-control has-feedback-left" id="inputSuccess4" placeholder="Email*" /> */}
                <input type="text" required className="form-control has-feedback-left" name="email"
                                        placeholder="Email*"
                                        value={this.state.email}
                                        onChange={this.handleUserInput.bind(this)}  />
                                        <span className="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                        <span className="form_error_msg" hidden={this.state.emailValid}>Invalid Email</span>
            </div>

            <div className="form-group has-feedback">
              {/*  <input type="text" className="form-control" id="inputSuccess5" placeholder="Phone*" /> */}
                <input type="phone" required className="form-control" name="phone"
                                        placeholder="Phone Number*"
                                        value={this.state.phone}
                                        onChange={this.handleUserInput.bind(this)}  />
                                        <span className="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                        <span className="form_error_msg" hidden={this.state.phoneValid}>Invalid Phone Number</span>
            </div>

            <div className="form-group has-feedback">
                <input type="text" required className="form-control has-feedback-left" name="country"
                                        placeholder="Country*" value={this.state.country}
                                        onChange={this.handleUserInput.bind(this)} />
                                        <span className="fa fa-country form-control-feedback left" aria-hidden="true"></span>
                                        <span className="form_error_msg" hidden={this.state.countryValid}>Invalid Country</span>
            </div>

            <div className="form-group has-feedback">
                <input type="text" required className="form-control has-feedback-left" name="stateValue"
                                        placeholder="State*" value={this.state.stateValue}
                                        onChange={this.handleUserInput.bind(this)} />
                                        <span className="fa fa-state form-control-feedback left" aria-hidden="true"></span>
                                        <span className="form_error_msg" hidden={this.state.stateValid}>Invalid state</span>
            </div>

            <div className="form-group has-feedback">
                <input type="text" required className="form-control has-feedback-left" name="city"
                                        placeholder="City*" value={this.state.city}
                                        onChange={this.handleUserInput.bind(this)} />
                                        <span className="fa fa-city form-control-feedback left" aria-hidden="true"></span>
                                        <span className="form_error_msg" hidden={this.state.cityValid}>Invalid city</span>
            </div>

            <div className="form-group has-feedback">
                <input type="text" required className="form-control has-feedback-left" name="pincode"
                                        placeholder="Pin Code*" value={this.state.pincode}
                                        onChange={this.handleUserInput.bind(this)}  />
                                        <span className="fa fa-pincode form-control-feedback left" aria-hidden="true"></span>
                                        <span className="form_error_msg" hidden={this.state.pincodeValid}>Invalid pincode</span>
            </div>

          </div>

            <div className="col-md-4 col-sm-5 col-xs-12 jrblk">

            <div className="form-group has-feedback">
                <input type="text" className="form-control has-feedback-left" name="address"
                                        placeholder=" Address* " value={this.state.address}
                                        onChange={this.handleUserInput.bind(this)} />
            </div>

              <div class="form-group">
                <textarea class="form-control" rows="3" id="comment" name="strongHuman" onBlur={this.onExitStrongHuman.bind(this)} placeholder="Why do you think you are a StrongHuman?*" value={this.state.strongHuman}
                onChange={this.handleUserInput.bind(this)} ></textarea>
              </div>

              <div className="form-group fgmb">
              <label className="control-label col-md-12 col-sm-12 col-xs-12">Preferred mode of communication</label>
              <div className="col-md-12 col-sm-12 col-xs-12 padd0">
              <div className="row">
                <div className="form-check col-md-3 col-sm-6 col-xs-12 pdl0">
                  <label className="form-check-label" htmlFor="check1">
                    <input type="checkbox" className="form-check-input" id="check1" name="mail" value={this.state.mail}
                    onChange={this.handleUserInput.bind(this)} />Email
                  </label>
                </div>
                <div className="form-check col-md-3 col-sm-6 col-xs-12">
                  <label className="form-check-label" htmlFor="check2">
                    <input type="checkbox" className="form-check-input" id="check2" name="whatsapp" value={this.state.whatsapp}
                     onChange={this.handleUserInput.bind(this)} />WhatsApp
                  </label>
                </div>
              </div>
              </div>
            </div>

            <div className="form-group">
              <label className="control-label col-md-2 col-sm-3 col-xs-12">Gender:</label>
              <div className="col-md-9 col-sm-9 col-xs-12 padd0">
                <div className="radio">
                  <label>
                    <input type="radio" id="optionsRadios1" value="Male" name="gender" onChange={this.handleUserInput.bind(this)} /> Male
                  </label>
                  <label>
                    <input type="radio" id="optionsRadios1" value="Female" name="gender" onChange={this.handleUserInput.bind(this)}/> Female
                  </label>
                  <label>
                    <input type="radio" id="optionsRadios1" value="Other" name="gender" onChange={this.handleUserInput.bind(this)}/> Other
                  </label>
                </div>
              </div>
              </div>

              <div className="ln_solid"></div>

              <div className="form-group">
                <div className="text-right">
                  <p><Link to={'/Signin'}>Already user, Signin!</Link></p>
                    <button type="submit" className="btn btn-success manual_btn" disabled={!this.state.formValid}>Submit</button>
                </div>
              </div>

            </div>
            <div className="col-md-2 col-sm-1 col-xs-12"></div>
          </form>
        </section>

        <section className="jsec03 hsec05">
          <div className="sec05blk1">
            <h2 className="text-center">JOIN US</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default JoinUs;
