import React, { Component } from 'react';
import YouTube from 'react-youtube';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import { Button, Modal, Glyphicon } from 'react-bootstrap';
import AuthService from './../../util/AuthService';
import Constant from './../../util/Constants';
import HeaderSignin from './HeaderSignin';
import $ from 'jquery';

declare var nameValue ;
declare var emailValue ;
declare var phoneValue ;
declare var subjectValue ;
declare var messageValue ;

const Auth = new AuthService();

class ContactUs extends Component {

  constructor (props) {
      super(props);
      this.state = {
        email: '',
        fullname: '',
        subject: '',
        message: '',
        phone: '',
        formErrors: {email: '', fullname: '', subject: '', message: '', phone: '',},
        emailValid: true,
        fullnameValid: true,
        subjectValid: false,
        messageValid: false,
        phoneValid: true,
        formValid: false,
        show: false,
        loggedin: false
      }
    }


    handleUserInput = (e) => {
      const name = e.target.name;
      const value = e.target.value;
      this.setState({[name]: value},
                    () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
      let fieldValidationErrors = this.state.formErrors;
      let emailValid = this.state.emailValid;
      let fullnameValid = this.state.fullnameValid;
      let subjectValid = this.state.subjectValid;
      let messageValid = this.state.messageValid;
      let phoneValid = this.state.phoneValid;
      value = value.replace(/^\s+|\s+$/gm,'');

      switch(fieldName) {
        case 'email':
          this.emailValue = value;
          emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
          fieldValidationErrors.email = emailValid ? '' : ' is invalid';
          break;
        case 'fullname':
          this.nameValue = value;
          fullnameValid = value.match(/^([a-zA-Z ])*$/i) && value.length >=1;
          fieldValidationErrors.fullname = fullnameValid ? '': ' should contain only alphabets';
          break;
        case 'subject':
          this.subjectValue = value;
          subjectValid = value.length >= 1;
          fieldValidationErrors.subject = subjectValid ? '': ' is invalid';
          break;
        case 'message':
          this.messageValue = value;
          messageValid = value.length >= 1;
          fieldValidationErrors.message = messageValid ? '': ' is invalid';
          break;
        case 'phone':
          this.phoneValue = value;
          phoneValid = value.length >= 0;
          fieldValidationErrors.phone = phoneValid ? '': 'number is invalid';
          break;
        default:
          break;
      }
      this.setState({formErrors: fieldValidationErrors,
                      emailValid: emailValid,
                      fullnameValid: fullnameValid,
                      subjectValid: subjectValid,
                      messageValid: messageValid,
                      phoneValid: phoneValid
                    }, this.validateForm);
    }

    validateForm() {
      this.setState({formValid: this.state.emailValid && this.state.fullnameValid && this.state.subjectValid && this.state.messageValid });
    }

    errorClass(error) {
      return(error.length === 0 ? '' : 'has-error');
    }

    handleSubmit = (evt) => {
      evt.preventDefault();
      fetch(Constant.backendurl+'/apicalls/contactus', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
           fullname: this.nameValue,
           email: this.emailValue,
           phone: this.phoneValue,
           subject: this.subjectValue,
           message: this.messageValue
        }),
      })
      .then(response => {return response.text();})
      .then(text => {
        if(text === 'successful'){
          this.setState({show:true, message: 'We have received your message.'});
        }
      }
    )
    .catch(error =>
      {
        console.log('parsing failed', error);
        this.setState({show:true, message: 'Something went wrong, please try again'});
        }
      )
}

  componentWillMount() {
    if (Auth.loggedIn()) {
      this.setState({"loggedin": true});
    }
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    let button;
    if(this.state.loggedin === false){
      button = <Header />;
    } else {
      button = <HeaderSignin history = {this.props.history} />;
    }
    return (
      <div>
        {button}
        <section className="hsec01 contactpg">
          <div className="container">
            <div className="text-center">
                <h2>Drop Your Message</h2>
                <p>For any queries & suggestions you can reach us by dropping an email at <a href="mailto:admin@shn.world">admin@shn.world</a>. If you are in hyderabad, you can visit our office.</p>
            </div>


            <div className="row">
                <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="row contact-wrap">
                    <form id="main-contact-form" className="contact-form" name="contact-form" onSubmit={this.handleSubmit}>

                            {/*<div className="form-group">
                                <label>Name *</label>
                                <input type="text" name="name" className="form-control" required />
                            </div>*/}
                            <div className={`form-group ${this.errorClass(this.state.formErrors.fullname)}`}>
                                      <input id="fullname" type="fullname"  required className="form-control" name="fullname"
                                        placeholder="Full Name*"
                                        value={this.state.fullname}
                                        onChange={this.handleUserInput} /> <span hidden={this.state.fullnameValid} className="form-error" > Name should contain only alphabets </span>
                            </div>
                            <div>
                              <Modal show={this.state.show}>
                                <Modal.Body>
                                  {this.state.message}
                                </Modal.Body>
                                <Modal.Footer>
                                  <Button onClick={() => this.setState({ show: false, fullname: "", email: "", subject: "", message: "", formValid: false,subjectValid: false,messageValid: false,emailbody: false })}>Close</Button>
                                </Modal.Footer>
                              </Modal>
                            </div>
                            {/*<div className="form-group">
                                <label>Email *</label>
                                <input type="email" name="email" className="form-control" required />
                            </div>*/}
                            <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                                      <input type="email" required className="form-control" name="email"
                                        placeholder="Email*"
                                        value={this.state.email}
                                        onChange={this.handleUserInput}  /> <span hidden={this.state.emailValid} className="form-error" > Email should be in the format example@example.com </span>
                            </div>
                            {/*<div className="form-group">
                                <label>Phone</label>
                                <input type="number" className="form-control" />
                            </div>
                            <div className={`form-group ${this.errorClass(this.state.formErrors.phone)}`}>
                                      <label htmlFor="phone">Phone</label>
                                      <input type="phone" className="form-control" name="phone"
                                        placeholder="Phone"
                                        value={this.state.phone}
                                        onChange={this.handleUserInput}  /> <span hidden={this.state.phoneValid} className="form-error" > Phone number should contain atleast 10 digits</span>
                            </div>*/}
                            <div className={`form-group ${this.errorClass(this.state.formErrors.subject)}`}>
                                      <input type="subject"  required className="form-control" name="subject"
                                        placeholder="Subject*"
                                        value={this.state.subject}
                                        onChange={this.handleUserInput}  />
                            </div>

                            {/*<div className="form-group">
                                <label>Subject *</label>
                                <input type="text" name="subject" className="form-control" required onChange={this.handleUserInput} />
                            </div>*/}

                            {/*<div className="form-group">
                                <label>Message *</label>
                                <textarea name="message" id="message" required className="form-control" rows="8"></textarea>
                            </div>*/}
                            <div className={`form-group ${this.errorClass(this.state.formErrors.message)}`}>
                                <textarea rows="8" type="message"  required className="form-control" name="message"
                                        placeholder="Message*"
                                        value={this.state.message}
                                        onChange={this.handleUserInput}  />
                            </div>
                            <div className="form-group">
                                <button type="submit" name="submit" className="btn btn-success manual_btn" disabled={!this.state.formValid}>Submit</button>
                                {/*<button type="submit" className="btn btn-primary" disabled={!this.state.formValid}>Sign up</button> */}
                            </div>
                    </form>
                </div>

                </div> {/* form col ends here */}
                <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3808.6256124787137!2d78.567501114438!3d17.333603788110842!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcba1f932659d0b%3A0x1d11a19691c8feb9!2sLaxmi+Enclave!5e0!3m2!1sen!2sin!4v1524998190410" width="100%" height="370" frameBorder="0" style={{border:0}} allowFullScreen></iframe>
                </div>
                </div> {/* map col ends here */}
            </div>{/* row ends here */}
          </div>{/* container ends here */}
        </section>
      </div>
    );
}
}

export default ContactUs;
