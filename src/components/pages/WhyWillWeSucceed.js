import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import hsec1Img from '../assets/images/hsec01_img.png';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
import Header from './Header';
import $ from 'jquery';

class WhyWillWeSucceed extends Component {

  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
      <Header selected = "WhyWillWeSucceed" history = {this.props.history} />
        <section className="wwsec01 hsec01">
          <div className="vs01Outcir">
            <div className="vs01Inncir">
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 ltblk">
              <p className="wws1bgtitle">WHY?</p>
              <p className="wws1txt">WHY WILL WE SUCCEED ?</p>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12 rtblk divfrnt">
              <p><strong>We abide by our specially designed core principles, core values and core competencies
which help keep us grounded and stay focused</strong></p>
            </div>
          </div>
          <div className="text-center"><img src={hsec1Img} alt="" /></div>
        </section>
        <section className="wwsec02 hsec02 hsec03">
        <div className="row">
          <div className="col-md-6 col-sm-6 col-xs-12 rtblk">
            <h2 className="hbgtitle">PRINCIPLES</h2>
            <div className="frntcir"><span className="verttxt">01</span></div>
            <div className="backcir"></div>
          </div>
          <div className="col-md-6 col-sm-6 col-xs-12 lftblk">
            <p className="wwesc02Title bltitle">OUR CORE PRINCPLES</p>
            <p><strong>1. Logical thinking<br/>
              2. Being rational and overcoming irrational fears<br/>
              3. Self awareness<br/>
              4. Being responsible towards yourself, family, friends, society and environment<br/>
              5. Knowing that you don’t know everything and being open to listen<br/>
              6. Free thinking<br/>
              7. Building the trust deficit!</strong></p>
          </div>
        </div>
        </section>
        <section className="wwsec03 hsec02 hsec04">
        <div className="row flrow">
          <div className="col-md-5 col-sm-5 col-xs-12 lftblk seccell02">
              <p className="wwesc02Title bltitle">OUR CORE VALUES</p>
              <p><strong>1. Ownership: Ownership of life, ownership of work<br/>
                  2. Confidence, belief and conviction : The 1st gate to greatness<br/>
                  3. Sincerity: sincerity towards your life<br/>
                  4. Dignity of work<br/>
                  5. Be solution oriented: we don’t complain. We find solution<br/>
                  6. Honesty: being honest to yourself is the best way to lead a quality life!<br/>
                  7. Accountability: be accountable to whatever you do!</strong></p>
          </div>
          <div className="col-md-7 col-sm-7 col-xs-12 rtblk seccell01">
            <h2 className="hbgtitle">VALUES</h2>
            <div className="frntcir"><span className="verttxt">02</span></div>
            <div className="backcir"></div>
          </div>
        </div>
        </section>
        <section className="wwsec04 wwsec02 hsec02 hsec03">
        <div className="row">
          <div className="col-md-6 col-sm-6 col-xs-12 rtblk">
            <h2 className="hbgtitle">COMPETENCIES</h2>
            <div className="frntcir"><span className="verttxt">03</span></div>
            <div className="backcir"></div>
          </div>
          <div className="col-md-6 col-sm-6 col-xs-12 lftblk">
            <p className="wwesc02Title bltitle">OUR CORE COMPETENCIES</p>
            <p><strong>1. Leadership: we create leaders!<br/>
                2. Execution: we do what we think!<br/>
                3. People: People are the only reality<br/>
                4. Culture: we are a cultural phenomenon!<br/>
                5. Design: Design the user experience!</strong></p>
          </div>
        </div>
        </section>
        <section className="wwsec05 hsec05">
          <div className="sec05blk1">
            <h2 className="text-center">Why Will We Succeed</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default WhyWillWeSucceed;
