import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import AuthWrapped from './../../util/AuthWrapped';
import AuthService from './../../util/AuthService';
import Footer from './Footer';
import $ from 'jquery';

class ThinkTanks extends Component {

  constructor (props) {
    super(props);
  }

 componentDidMount(){
  $('html,body').scrollTop(0);
}
  render() {
    return (
      <div>
        <Header history = {this.props.history} />
        <section className="ttsec01 btsec01 acsec01 wwsec01 hsec01">
          <div className="vs01Outcir">
            <div className="vs01Inncir">
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 ltblk">
              <p className="wws1bgtitle">TANK</p>
              <p className="wws1txt">THINK TANK</p>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12 rtblk divfrnt">
              <p><strong>Thinktank is SHN’s brain which visualises various aspects of the
                  society according to its principles and values.<br/>
                  ThinkTank focuses on researching and developing solutions
                  to challenges in various domains. </strong></p>
            </div>
          </div>
          <div className="text-center"><img src={hsec1Img} alt="" /></div>
        </section>

        <section className="ttsec02 btsec02 fasec02 wwsec02 hsec02 hsec03">
          <div className="vs02Outcir">
            <div className="vs02Inncir"></div>
          </div>
          <p className="wws1bgtitle">DOMAINS</p>
          <p className="wws1txt">DOMAINS</p>
          <div className="faTitle1">
            <p className="faTxt1">01</p>
            <p className="faTxt2">Food and Agriculture</p>
          </div>
          <div className="faTitle2">
            <p className="faTxt1">02</p>
            <p className="faTxt2">Sports & Education</p>
          </div>
          <div className="faTitle3">
            <p className="faTxt1">03</p>
            <p className="faTxt2">Health Care</p>
          </div>
          <div className="faTitle4">
            <p className="faTxt1">04</p>
            <p className="faTxt2">Infrastructure</p>
          </div>
          <div className="faTitle5">
            <p className="faTxt1">05</p>
            <p className="faTxt2">Media & Entertainment</p>
          </div>
          <div className="faTitle6">
            <p className="faTxt1">06</p>
            <p className="faTxt2">Governance</p>
          </div>
          <div className="faTitle7">
            <p className="faTxt1">07</p>
            <p className="faTxt2">Science and Technology</p>
          </div>
          <div className="faTitle8">
            <p className="faTxt1">08</p>
            <p className="faTxt2">Tourism</p>
          </div>
          <div className="faTitle9">
            <p className="faTxt1">09</p>
            <p className="faTxt2">Energy</p>
          </div>
        </section>

        <section className="ttsec03 btsec03 acsec05 wwsec05 hsec05">
          <div className="text-center sec05blk1">
            <h2>THINK TANK</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default ThinkTanks;
