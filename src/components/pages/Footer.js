import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import fFacebook from '../assets/images/footerFbIcon.png';
import fInstagram from '../assets/images/footerInstaIcon.png';
import fLinkedin from '../assets/images/footerLinkedinIcon.png';
import fTwitter from '../assets/images/footerTwitterIcon.png';
import uparr from '../assets/images/uparr.png';

class Footer extends Component {

  render() {
    return (
      <div>
        <div className="smedia text-center">
            <a href="https://www.facebook.com/Stronghumannetwork/" target="_blank" rel="noopener noreferrer"><img src={fFacebook} alt="Facebook" /></a>
            <a href="https://twitter.com/SHN_world" target="_blank" rel="noopener noreferrer"><img src={fTwitter} alt="Twitter" /></a>
            <a href="https://www.linkedin.com/company/stronghumannetwork/" target="_blank" rel="noopener noreferrer"><img src={fLinkedin} alt="Linkedin" /></a>
            <a href="https://www.instagram.com/shn.world" target="_blank" rel="noopener noreferrer"><img src={fInstagram} alt="Instagram" /></a>
        </div>

      </div>
    );
  }
}

export default Footer;
