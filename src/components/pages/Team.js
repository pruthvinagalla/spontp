import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import Anuradha from '../assets/images/Anuradha.jpg';
import Gauthami from '../assets/images/Gauthami.jpg';
import Kapeesh from '../assets/images/Kapeesh.jpg';
import Karthik from '../assets/images/Karthik.jpg';
import Narayan from '../assets/images/Narayan.jpg';
import Naveen from '../assets/images/Naveen.jpg';
import Pavithra from '../assets/images/Pavithra.jpg';
import Priyanka from '../assets/images/Priyanka.jpg';
import RajKumar from '../assets/images/RajKumar.jpg';
import Sanjana from '../assets/images/Sanjana.jpg';
import Snigdha from '../assets/images/Snigdha.jpg';
import Sravani from '../assets/images/Sravani.jpg';
import Srinu from '../assets/images/Srinu.jpg';
import Surya from '../assets/images/Surya.jpg';
import Swetha from '../assets/images/Swetha.jpg';
import Vishwesh from '../assets/images/Vishwesh.jpg';
import Yashashwini from '../assets/images/Yashashwini.jpg';
import Sainath from '../assets/images/Sainath.jpg';
import Srigiri from '../assets/images/Srigiri.jpg';
import Pavan from '../assets/images/Pavan.jpg';
import Saidutt from '../assets/images/Saidutt.jpg';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
import $ from 'jquery';

class Team extends Component {
  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header selected = "Team" history = {this.props.history} />
        <section className="teamsec01 btsec01 acsec01 wwsec01 hsec01">
          <div className="vs01Outcir">
            <div className="vs01Inncir">
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 ltblk">
              <p className="wws1bgtitle">EMAKER</p>
              <p className="wws1txt">CHANGEMAKER</p>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12 rtblk divfrnt">
              <p><strong>ChangeMakers are people who are willing to improve themselves!<br/>
                  ChangeMakers believe that society is creation of humans and not the other way around
                  ChangeMakers help community around them to be a StrongHuman
                  ChangeMakers believe they can change the world!<br/>
                  ChangeMakers are StrongHumans!</strong></p>
            </div>
          </div>
          <div className="text-center"><img src={hsec1Img} alt="" /></div>
        </section>

        <section className="teamsec02 hsec02 hsec03">
        <div className="teamCont">
            <p className="cmtxt">MEET OUR CHANGEMAKERS</p>
            <div className="teamM">
              <img src={Karthik} alt="" />
              <p className="cmName">Kartik</p>
              <p className="cmDes">Managing Director, SME in Relationship Management</p>
            </div>
            <div className="teamM">
              <img src={Yashashwini} alt="" />
              <p className="cmName">Dr. Yasaswini</p>
              <p className="cmDes">Chief Happiness Officer</p>
            </div>
            <div className="teamM">
              <img src={Narayan} alt="" />
              <p className="cmName">Narayan</p>
              <p className="cmDes">Senior Project Manager/ SME in Confidence</p>
            </div>
            <div className="teamM">
              <img src={Vishwesh} alt="" />
              <p className="cmName">Krishna Vishwesh</p>
              <p className="cmDes">Product Manager/ SME in Self-awareness & Confidence</p>
            </div>
            <div className="teamM">
              <img src={Kapeesh} alt="" />
              <p className="cmName">Kapeesh Saxena</p>
              <p className="cmDes">Employee Engagement Lead/ SME in Relationship Management</p>
            </div>
            <div className="teamM">
              <img src={Swetha} alt="" />
              <p className="cmName">Sweta</p>
              <p className="cmDes">Marketing Manager/ SME in Self-awareness & Confidence</p>
            </div>
            <div className="teamM">
              <img src={Anuradha} alt="" />
              <p className="cmName">Anuradha</p>
              <p className="cmDes">HR Management Trainee/ SME in Self-awareness, Emotional Intelligence, Relationship Management</p>
            </div>
            <div className="teamM">
              <img src={Gauthami} alt="" />
              <p className="cmName">Gauthami</p>
              <p className="cmDes">Education Research Associate/ SME in Confidence</p>
            </div>
            <div className="teamM">
              <img src={Naveen} alt="" />
              <p className="cmName">Naveen</p>
              <p className="cmDes">ChangeMaker Manager/ SME in Emotional Intelligence</p>
            </div>
            <div className="teamM">
              <img src={Pavithra} alt="" />
              <p className="cmName">Pavithra</p>
              <p className="cmDes">HR manager</p>
            </div>
            <div className="teamM">
              <img src={Priyanka} alt="" />
              <p className="cmName">Priyanka</p>
              <p className="cmDes">Business Development Manager</p>
            </div>
            <div className="teamM">
              <img src={RajKumar} alt="" />
              <p className="cmName">Raaj Kumar</p>
              <p className="cmDes">Fashion Category Manager/ SME in Fashion</p>
            </div>
            <div className="teamM">
              <img src={Sanjana} alt="" />
              <p className="cmName">Sanjana</p>
              <p className="cmDes">Executive Assistant to CEO</p>
            </div>
            <div className="teamM">
              <img src={Snigdha} alt="" />
              <p className="cmName">Snigdha</p>
              <p className="cmDes">Employee Engagement Manager, SME in Self-awareness</p>
            </div>
            <div className="teamM">
              <img src={Srinu} alt="" />
              <p className="cmName">Srinu</p>
              <p className="cmDes">Database Consultant</p>
            </div>
            <div className="teamM">
              <img src={Surya} alt="" />
              <p className="cmName">Surya Kiran</p>
              <p className="cmDes">Wellness Manager/ SME in Wellness</p>
            </div>
        </div>
        </section>

        <section className="teamsec08 btsec03 acsec05 wwsec05 hsec05">
          <div className="text-center sec05blk1">
            <h2>TEAM</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default Team;
