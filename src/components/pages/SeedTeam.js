import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import Anuradha from '../assets/images/Anuradha.jpg';
import Gauthami from '../assets/images/Gauthami.jpg';
import Kapeesh from '../assets/images/Kapeesh.jpg';
import Karthik from '../assets/images/Karthik.jpg';
import Narayan from '../assets/images/Narayan.jpg';
import Naveen from '../assets/images/Naveen.jpg';
import Pavithra from '../assets/images/Pavithra.jpg';
import Priyanka from '../assets/images/Priyanka.jpg';
import RajKumar from '../assets/images/RajKumar.jpg';
import Sanjana from '../assets/images/Sanjana.jpg';
import Snigdha from '../assets/images/Snigdha.jpg';
import Sravani from '../assets/images/Sravani.jpg';
import Srinu from '../assets/images/Srinu.jpg';
import Surya from '../assets/images/Surya.jpg';
import Swetha from '../assets/images/Swetha.jpg';
import Vishwesh from '../assets/images/Vishwesh.jpg';
import Yashashwini from '../assets/images/Yashashwini.jpg';
import Sainath from '../assets/images/Sainath.jpg';
import Srigiri from '../assets/images/Srigiri.jpg';
import Pavan from '../assets/images/Pavan.jpg';
import Saidutt from '../assets/images/Saidutt.jpg';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
//import HeaderSignin from './HeaderSignin';
import $ from 'jquery';


class SeedTeam extends Component {
  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header selected = "SeedTeam" history = {this.props.history} />
        <section className="teamsec03 btsec01 acsec01 wwsec01 hsec01">
          <div className="vs01Outcir">
            <div className="vs01Inncir">
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 ltblk">
              <p className="wws1bgtitle">TEAM</p>
              <p className="wws1txt">SEED TEAM</p>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12 rtblk divfrnt">
              <p><strong>Seed team has conceptualised StrongHumanNetwork.<br/>
                  They know exactly why StrongHumanNetwork has started and have visualised the
                  end result of this revolution!</strong></p>
            </div>
          </div>
          <div className="text-center"><img src={hsec1Img} alt="" /></div>
        </section>

        <section className="teamsec teamsec04 hsec02">
            <p className="tmbgttl">SNM</p>
            <div className="tmline"></div>
            <div className="tmdotcir1"></div>
            <div className="tmdotcir2"></div>
            <div className="tmoutcir"><img src={Sainath} alt="" /></div>
            <p className="teamttl">Sainath Maharatta</p>
            <p className="teamtxt">With SHN he has found the purpose of his life.</p>
        </section>

        <section className="teamsec teamsec05 teamsec04 hsec01">
            <p className="tmbgttl">SGS</p>
            <div className="tmline"></div>
            <div className="tmdotcir1"></div>
            <div className="tmdotcir2"></div>
            <div className="tmoutcir"><img src={Srigiri} alt="" /></div>
            <p className="teamttl">Srigirisai Jala</p>
            <p className="teamtxt">He feeds on knowledge and the idea of learning and sharing it throughout his life journey.</p>
        </section>

        <section className="teamsec teamsec06 teamsec04 hsec02">
            <p className="tmbgttl">PK</p>
            <div className="tmline"></div>
            <div className="tmdotcir1"></div>
            <div className="tmdotcir2"></div>
            <div className="tmoutcir"><img src={Pavan} alt="" /></div>
            <p className="teamttl">Pavan Kaligotla</p>
            <p className="teamtxt">He believes everyone should be a StrongHuman and that it is us who create our destiny. </p>
        </section>

        <section className="teamsec teamsec07 teamsec04 hsec01">
            <p className="tmbgttl">SD</p>
            <div className="tmline"></div>
            <div className="tmdotcir1"></div>
            <div className="tmdotcir2"></div>
            <div className="tmoutcir"><img src={Saidutt} alt="" /></div>
            <p className="teamttl">Saidutt Karnapu</p>
            <p className="teamtxt">Conscious soul and a creative being, constantly exploring the universe within,
            aspire to unfold the mysteries of human excellence to an entire mankind.</p>
        </section>

        <section className="teamsec08 btsec03 acsec05 wwsec05 hsec05">
          <div className="text-center sec05blk1">
            <h2>SEED TEAM</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default SeedTeam;
