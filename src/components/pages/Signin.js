import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import AuthService from './../../util/AuthService';
import { Button, Modal } from 'react-bootstrap';

class Signin extends Component {
  constructor (props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      emailValid: true,
      passwordValid: false,
      showModal: false,
      showModal2: false
    }
    this.Auth = new AuthService();
  }

  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},
                () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;

    switch(fieldName) {
      case 'email':
        this.emailValue = value;
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        break;
      case 'password':
        this.passwordValue = value;
        passwordValid = value.length >= 1;
        break;
      default:
        break;
    }
    this.setState({ emailValid: emailValid,
                  passwordValid: passwordValid
                }, this.validateForm);
  }

  validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.passwordValid});
  }

  componentWillMount(){
    if(this.Auth.loggedIn())
      this.props.history.replace('/HomeSignIn');
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    this.Auth.login(this.emailValue,this.passwordValue)
    .then(res =>{
               this.props.history.replace('/HomeSignIn');
          })
    .catch(err =>{
      if(err.response !== undefined ){
        this.setState({ showModal: true })
        if(err.response.statusText === 'Unauthorized')
          this.setState({errorMessage: 'Email or password is invalid'})
        else
          this.setState({errorMessage: 'Something went wrong, please try again later'})
        } else {
          this.setState({ showModal: true, errorMessage: 'Some issue with server' });
        }
      })
   //  fetch(Constant.backendurl+'/apicalls/reg', {
   //    method: 'POST',
   //    headers: {
   //      Accept: 'application/json',
   //      'Content-Type': 'application/json',
   //    },
   //    body: JSON.stringify({
   //      email: this.emailValue,
   //      password: this.passwordValue
   //    }),
   //  })
   //  .then(response => {return response.text();})
   //  .then( text => {
   //    if(text === 'valid')
   //      window.location.replace('/#/Founders/');
   //    else{
   //      this.setState({ showModal: true })
   //      this.setState({errorMessage: text})
   //    }
   //  })
   // .catch(error => {
   //                   console.log('parsing failed', error);
   //                   this.setState({ showModal2: true })
   //                   this.setState({errorMessage2: "Something went wrong, please try again"})
   //                  }
   //      )
  }
  render() {
    return (
      <div>
        <Header />
        <div>
          <Modal show={this.state.showModal}>
            <Modal.Body>
              <span> {this.state.errorMessage} </span>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.setState({ showModal: false, email: "", password: "", formValid: false })}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
        <div>
          <Modal show={this.state.showModal2}>
            <Modal.Body>
              <span> {this.state.errorMessage2} </span>
            </Modal.Body>
            <Modal.Footer>
              <Link to={'/'}>  <Button onClick={() => this.setState({ showModal2: false, email: "", password: "", formValid: false })}>Close</Button></Link>
            </Modal.Footer>
          </Modal>
        </div>
            <div className="col-md-12 col-xs-12 forgotpassworddiv">
              <h4 className="text-center">Sign in to enter our <b><span className="grn">World</span></b></h4>
              <form className="form-horizontal form-label-left input_mask" onSubmit={this.handleSubmit.bind(this)}>
                <div className="col-md-3 col-sm-2 col-xs-12"></div>
                <div className="col-md-6 col-sm-8 col-xs-12 padd0">
                  <div className="form-group prel">
                    {/*}  <input type="email" className="form-control" placeholder="Email Id" /> */}
                      <input type="text" name="email"className="form-control" placeholder="Email Id" required value={this.state.email} onChange={this.handleUserInput.bind(this)}  />
                      <span className="form_error_msg_signin" hidden={this.state.emailValid}>Invalid Email</span>
                  </div>

                  <div className="form-group prel">
                    <input type="password" name="password" className="form-control" placeholder="Password" required value={this.state.password} onChange={this.handleUserInput.bind(this)} />
                  </div>

                  <div className="form-group fpwsp">
                    <p><span className="fleft"><Link to={'/ForgotPassword'}>Forgot account or password?</Link></span><span className="fright"><Link to={'/JoinUs'}>If new user, register!</Link></span></p>
                    <div className="text-right">
                      <button type="submit" className="btn btn-success manual_btn" disabled={!this.state.formValid}>Submit</button>
                    </div>
                  </div>

                </div>
                <div className="col-md-3 col-sm-2 col-xs-12"></div>
              </form>
            </div>
          </div>
    );
  }
}

export default Signin;
