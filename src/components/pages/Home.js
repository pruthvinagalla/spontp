import React, { Component } from 'react';
import YouTube from 'react-youtube';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import { Button, Modal,Glyphicon } from 'react-bootstrap';
import AuthService from './../../util/AuthService';
import $ from 'jquery';

class Home extends Component {
  constructor (props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      emailValid: true,
      passwordValid: false,
      showModal: false,
      showModal2: false,
      showModal3: false
    }
    this.Auth = new AuthService();
  }

  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},
                () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;

    switch(fieldName) {
      case 'email':
        this.emailValue = value;
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        break;
      case 'password':
        this.passwordValue = value;
        passwordValid = value.length >= 1;
        break;
      default:
        break;
    }
    this.setState({ emailValid: emailValid,
                  passwordValid: passwordValid
                }, this.validateForm);
  }

  validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.passwordValid});
  }

  componentWillMount(){
    if(this.Auth.loggedIn())
      this.props.history.replace('/HomeSignIn');
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    this.Auth.login(this.emailValue,this.passwordValue)
    .then(res =>{
               this.props.history.replace('/HomeSignIn');
          })
    .catch(err =>{
            this.setState({ showModal: true })
            if(!err.response.statusText){
              this.setState({errorMessage: 'Some issue with server'});
            }
             else {
               if(err.response.statusText === 'Unauthorized')
                  this.setState({errorMessage: 'Email or password is invalid'})
               else
                  this.setState({errorMessage: 'Something went wrong, please try again later'})
            }
          })
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    const opts = {
      height: '500',
      width: '100%',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 1,
        modestbranding: 1,
        rel : 0
      }
    };

    return (
      <div>
        <Header />

        {/* modal starts here */}

        <div>
          <Modal show={this.state.showModal}>
            <Modal.Body>
              <span> {this.state.errorMessage} </span>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.setState({ showModal: false, email: "", password: "", formValid: false })}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
        <div>
          <Modal show={this.state.showModal2}>
            <Modal.Body>
              <span> {this.state.errorMessage2} </span>
            </Modal.Body>
            <Modal.Footer>
              <Link to={'/'}>  <Button onClick={() => this.setState({ showModal2: false, email: "", password: "", formValid: false })}>Close</Button></Link>
            </Modal.Footer>
          </Modal>
        </div>
        <div>
          <Modal bsSize = "large" show={this.state.showModal3} onHide={() => this.setState({ showModal3: false})} >
          <Modal.Header closeButton>
            <Modal.Title>{this.state.contentHead}</Modal.Title>
          </Modal.Header>
          <Modal.Body className="qmodal">
          <div className="youtubevdo">
          <YouTube classname="myVideo"
            videoId="OrD7swP-qXI"
            opts={opts}
            onReady={this._onReady}
          />
          </div>
          </Modal.Body>
        { /*  <Modal.Footer>
              {/*<Button onClick={() => this.setState({ showModal: false})}>Close</Button>
              <Link to={'/'} className="btn btn-default">Close</Link>
            <Button className="btn btn-default" onClick={() => this.setState({ showModal3: false})}>Close</Button>
          </Modal.Footer> */ }
          </Modal>
          </div>

        {/* modal ends here */}


        <section className="hsec01 hmpg">
          <span onClick={() => this.setState({showModal3: true})} className="homeBgTransparent"></span>
          <div className="hmltblk">
            <h1>Are you a StrongHuman?</h1>
          </div>

        </section>

      </div>
    );
  }
}

export default Home;
