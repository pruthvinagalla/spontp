import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import hsec1Img from '../assets/images/hsec01_img.png';
import Codetop from '../assets/images/codetop.png';
import Codebottom from '../assets/images/codebottom.png';
import AuthWrapped from './../../util/AuthWrapped';
import $ from 'jquery';

class StrongHuman extends Component {

  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header selected = "StrongHuman" history = {this.props.history} />
        <section className="shsec01 hsec01">
          <h1 className="text-center">Who is a StrongHuman ?</h1>
          <div className="text-center"><span className="sech1Line"><span className="sech1cir"></span></span></div>
          <p className="shsec01p text-center">
          A StrongHuman is a <strong>confident</strong>, <strong>self aware individual</strong> with a <strong>fair world exposure</strong> and <strong>direction in life</strong>, who is willing to <strong>work
          on oneself</strong> with a <strong>set of tools</strong> to achieve one's <strong>excellence potential</strong>!
          </p>
          <div className="text-center"><img className="dwarr" src={hsec1Img} alt="" /></div>
        </section>
        <section className="shsec02 hsec02">
          <div className="row flrow">
            <div className="col-md-5 col-sm-5 col-xs-12 lftblk seccell02">
              <p className="sec2pTitle">Confidence and belief in yourself is the first
                  step for achieving anything in life.</p>
              <div className="text-center divline"><span className="sech2Line"><span className="sech2cir"></span></span></div>
              <p>It is not just the belief in oneself and ones capabilities but also a self fulfilling
                prophecy. If you believe you can do something you will eventually find a way to do it
                and if you believe you can’t do anything, you will not even try and hence you will
                never do it.</p>
            </div>
            <div className="col-md-7 col-sm-7 col-xs-12 rtblk seccell01">
              <h2 className="hbgtitle">CONFIDENCE</h2>
              <div className="frntcir"><span className="verttxt">01</span></div>
              <div className="backcir"></div>
            </div>
          </div>
        </section>
        <section className="shsec03 hsec02 hsec03">
        <div className="row">
          <div className="col-md-7 col-sm-7 col-xs-12 rtblk">
            <h2 className="hbgtitle">AWARENESS</h2>
            <div className="frntcir"><span className="verttxt">02</span></div>
            <div className="backcir"></div>
          </div>
          <div className="col-md-5 col-sm-5 col-xs-12 lftblk">
            <p className="sec2pTitle text-right">Self-awareness is the single most important
                aspect responsible for an individual’s success.</p>
            <div className="text-center divline"><span className="sech2Line"><span className="sech2cir"></span></span></div>
            <p className="text-right">It is just about understanding ones emotions , thoughts and behaviour and acting
                consciously in a constructive way. Though it is such an important aspect, sadly it has not
                been given enough recognition at a society level across. Only when we take this concept to the mass we will unlock the key to societal change.</p>
          </div>
        </div>
        </section>
        <section className="shsec04 hsec02 hsec04">
        <div className="row flrow">
          <div className="col-md-5 col-sm-5 col-xs-12 lftblk seccell02">
            <p className="sec2pTitle">Understanding the world around you is a crucial aspect
                which when coupled with confidence and self awareness
                gives an individual a direction in life.
            </p>
            <div className="text-center divline"><span className="sech2Line"><span className="sech2cir"></span></span></div>
            <p>In this era where access to information is easy, it is easier to get an information overload
                and at the end do nothing with it. This is a major prevalent issue which needs to be understood
                and corrected at an individual level. It is important to chose to listen to the right and
                limited information which will help you interpret better.</p>
          </div>
          <div className="col-md-7 col-sm-7 col-xs-12 rtblk seccell01">
            <h2 className="hbgtitle">DIRECTION</h2>
            <div className="frntcir"><span className="verttxt">03</span></div>
            <div className="backcir"></div>
          </div>
        </div>
        </section>
        <section className="shsec05 hsec02 hsec03">

          <img className="code_top" src={Codetop} alt="" />
          <img className="code_bottom" src={Codebottom} alt="" />

          <h2>EXCELLENCE</h2>
          <p>An individual with an understanding of above concepts
              with the willingness to work on himself/herself is bound
              to achieve their excellence potential and lead a very
              highly successful , happy and satisfied life.</p>
        </section>
        <section className="shsec06 hsec02 hsec04">
        <div className="row flrow">
          <div className="col-md-5 col-sm-5 col-xs-12 lftblk seccell02">
            <p className="sec2pTitle">StrongHumanNetwork is on a mission to create
                StrongHumans and it will spread until everyone on this
                planet becomes a part of it!
            </p>
            <div className="text-center divline"><span className="sech2Line"><span className="sech2cir"></span></span></div>
            <p>With the technological advancements such as Artificial
intelligence and machine learning, machines are becoming
smarter and the
humanity needs to understand that to
effectively use these technologies for the benefit of
everyone we all ne
ed to buckle up and be StrongHumans</p>
          </div>
          <div className="col-md-7 col-sm-7 col-xs-12 rtblk seccell01">
            <h2 className="hbgtitle">MISSION</h2>
            <div className="frntcir"><span className="verttxt">05</span></div>
            <div className="backcir"></div>
          </div>
        </div>
        </section>
        <section className="shsec07 hsec05">
          <div className="sec05blk1">
            <h2 className="text-center">STRONGHUMANS</h2>
            <h3 className="text-center">Only StrongHumans will survive on this planet!</h3>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default StrongHuman;
