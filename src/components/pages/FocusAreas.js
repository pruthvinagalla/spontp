import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Header from './Header';
import hsec1Img from '../assets/images/hsec01_img.png';
import Navbar from 'react-bootstrap/lib/Navbar.js';
import Nav from 'react-bootstrap/lib/Nav.js';
import NavItem from 'react-bootstrap/lib/NavItem.js';
import AuthWrapped from './../../util/AuthWrapped';
import Footer from './Footer';
//import HeaderSignin from './HeaderSignin';
import $ from 'jquery';

class FocusAreas extends Component {

  constructor (props) {
    super(props);
  }

  componentDidMount(){
   $('html,body').scrollTop(0);
  }

  render() {
    return (
      <div>
        <Header selected = "FocusAreas" history = {this.props.history} />
        <section className="fasec01 wwsec01 hsec01">
          <div className="vs01Outcir">
            <div className="vs01Inncir">
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12 ltblk">
              <p className="wws1bgtitle">AREAS</p>
              <p className="wws1txt">FOCUS AREAS</p>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12 rtblk divfrnt">
              <p><strong>We are a personality development focus company envisioning to make
                  personality development sexy and take it to every person on the planet.<br/>
                  We have a holistic approach towards personality development which makes it
                  very interesting for anyone.<br/>
                  Your personality depends on what you think, eat, breath, listen, work, wear and
                  how you behave with people around you.<br/>
                  We provide services & products which will positively impact your personality.</strong></p>
            </div>
          </div>
          <div className="text-center"><img src={hsec1Img} alt="" /></div>
        </section>

        <section className="fasec02 wwsec02 hsec02 hsec03">
          <div className="vs02Outcir">
            <div className="vs02Inncir"></div>
          </div>
          <div className="faTitle1">
            <p className="faTxt1">01</p>
            <p className="faTxt2">Confidence Building</p>
          </div>
          <div className="faTitle2">
            <p className="faTxt1">02</p>
            <p className="faTxt2">Self Awareness</p>
          </div>
          <div className="faTitle3">
            <p className="faTxt1">03</p>
            <p className="faTxt2">Emotional Intelligence</p>
          </div>
          <div className="faTitle4">
            <p className="faTxt1">04</p>
            <p className="faTxt2">Wellness</p>
          </div>
          <div className="faTitle5">
            <p className="faTxt1">05</p>
            <p className="faTxt2">Relationship Management</p>
          </div>
          <div className="faTitle6">
            <p className="faTxt1">06</p>
            <p className="faTxt2">Fashion</p>
          </div>
          <div className="faTitle7">
            <p className="faTxt1">07</p>
            <p className="faTxt2">Career Planning</p>
          </div>
        </section>

        <section className="fasec03 wwsec05 hsec05">
          <div className="sec05blk1">
            <h2 className="text-center">FOCUS AREAS</h2>
          </div>
          <div className="sec05blk2">
            <h3 className="text-center">Keep in touch</h3>
            <Footer />
          </div>
        </section>
      </div>
    );
  }
}

export default FocusAreas;
