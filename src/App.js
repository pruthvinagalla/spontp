import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Home from './components/pages/Home';
import FocusAreas from './components/pages/FocusAreas';
import JoinUs from './components/pages/JoinUs';
import ContactUs from './components/pages/ContactUs';
import StrongHuman from './components/pages/StrongHuman';
import Team from './components/pages/Team';
import SeedTeam from './components/pages/SeedTeam';
import VisualiseTheFuture from './components/pages/VisualiseTheFuture';
import WhatDoWeDo from './components/pages/WhatDoWeDo';
import WhyWillWeSucceed from './components/pages/WhyWillWeSucceed';
import Academy from './components/pages/Academy';
import BusinessTransformation from './components/pages/BusinessTransformation';
import ThinkTanks from './components/pages/ThinkTanks';
import SignupThankyou from './components/pages/SignupThankyou';
import Signin from './components/pages/Signin';
import CreatePassword from './components/pages/CreatePassword';
import ForgotPassword from './components/pages/ForgotPassword';
import ForgotPasswordInfo from './components/pages/ForgotPasswordInfo';
import Auth from './components/pages/Auth';
import HomeSignIn from './components/pages/HomeSignIn';

import './App.css';

class App extends Component {
  render() {
    return (
      <HashRouter>
        <div>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route exact path='/FocusAreas' component={FocusAreas} />
              <Route exact path='/JoinUs' component={JoinUs} />
              <Route exact path='/ContactUs' component={ContactUs} />
              <Route exact path='/StrongHuman' component={StrongHuman} />
              <Route exact path='/Team' component={Team} />
              <Route exact path='/SeedTeam' component={SeedTeam} />
              <Route exact path='/VisualiseTheFuture' component={VisualiseTheFuture} />
              <Route exact path='/WhatDoWeDo' component={WhatDoWeDo} />
              <Route exact path='/WhyWillWeSucceed' component={WhyWillWeSucceed} />
              <Route exact path='/Academy' component={Academy} />
              <Route exact path='/BusinessTransformation' component={BusinessTransformation} />
              <Route exact path='/ThinkTanks' component={ThinkTanks} />
              <Route exact path='/SignupThankyou' component={SignupThankyou} />
              <Route exact path='/Signin' component={Signin} />
              <Route exact path='/Auth/:ref' component={Auth} />
              <Route exact path='/CreatePassword/:ref' component={CreatePassword} />
              <Route exact path='/ForgotPassword' component={ForgotPassword} />
              <Route exact path='/ForgotPasswordInfo' component={ForgotPasswordInfo} />
              <Route exact path='/HomeSignIn' component={HomeSignIn} />

            </Switch>
        </div>
      </HashRouter>
    );
  }
}

export default App;
