import React, { Component } from 'react';
import AuthService from './AuthService';
import Header from './../components/pages/Header';
import {Button, Alert,Modal} from 'react-bootstrap';

export default function withAuth(AuthComponent) {
  const Auth = new AuthService();
  return class AuthWrapped extends Component {
    constructor() {
      super();
      this.state = {
        user: null,
        showModal: true
      }
    }

      handleDismiss() {
         this.setState({ showModal: false });
         this.props.history.replace('/Signin');
       }

    componentWillMount() {
      if (!Auth.loggedIn()) {
        //alert("Kindly Sign In to know more");
        //this.props.history.replace('/Signin');
      }
      else {
        try {
            const profile = Auth.getProfile()
            this.setState({
                user: profile
            })
        }
        catch(err){
            Auth.logout()
            this.props.history.replace('/Signin')
        }
      }
    }// end of componentWillMount

    render() {
      if(this.state.user){
        return (
          <AuthComponent history={this.props.history} user={this.state.user} />
        );
      } else {
        return (
          <div>
          <Header />
          <div>
            <Modal className="errmodal" backdrop = "static" show={this.state.showModal} onHide={() => this.setState({ showModal: false})}>
              <Modal.Body>
                <span> Kindly Signin to Know More! </span>
              </Modal.Body>
              <Modal.Footer>
                <Button bsStyle="primary" onClick={this.handleDismiss.bind(this)}>Signin</Button>
              </Modal.Footer>
            </Modal>
          </div>
          {/*<Alert bsStyle="info" onDismiss={this.handleDismiss.bind(this)} >
              <h4>Kindly sign in to know more!</h4>
              <p>
                <Button bsStyle="primary" onClick={this.handleDismiss.bind(this)}>Sign in</Button>
              </p>
            </Alert>*/}
          </div>
        );
      }
    }
  }
}
